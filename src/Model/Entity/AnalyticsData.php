<?php
namespace FinlayDaG33k\Analytics\Model\Entity;

use Cake\ORM\Entity;

/**
 * AnalyticsData Entity
 *
 * @property int $id
 * @property int $token_id
 * @property string $ip
 * @property string $country
 * @property string $useragent
 * @property string $url
 * @property string $settings
 * @property \Cake\I18n\FrozenTime $date
 *
 * @property \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken $analytics_token
 */
class AnalyticsData extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'token_id' => true,
        'ip' => true,
        'country' => true,
        'useragent' => true,
        'url' => true,
        'settings' => true,
        'date' => true,
        'analytics_token' => true
    ];
}
