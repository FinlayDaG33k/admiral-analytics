<?php
namespace FinlayDaG33k\Analytics\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AnalyticsData Model
 *
 * @property \FinlayDaG33k\Analytics\Model\Table\AnalyticsTokensTable&\Cake\ORM\Association\BelongsTo $AnalyticsTokens
 *
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData get($primaryKey, $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData newEntity($data = null, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData[] newEntities(array $data, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData[] patchEntities($entities, array $data, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsData findOrCreate($search, callable $callback = null, $options = [])
 */
class AnalyticsDataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('analytics_data');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AnalyticsTokens', [
            'foreignKey' => 'token_id',
            'joinType' => 'INNER',
            'className' => 'FinlayDaG33k/Analytics.AnalyticsTokens'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('ip')
            ->requirePresence('ip', 'create')
            ->notEmptyString('ip');

        $validator
            ->scalar('country')
            ->requirePresence('country', 'create')
            ->notEmptyString('country');

        $validator
            ->scalar('useragent')
            ->requirePresence('useragent', 'create')
            ->notEmptyString('useragent');

        $validator
            ->scalar('url')
            ->requirePresence('url', 'create')
            ->notEmptyString('url');

        $validator
            ->scalar('settings')
            ->requirePresence('settings', 'create')
            ->notEmptyString('settings');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmptyDateTime('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['token_id'], 'AnalyticsTokens'));

        return $rules;
    }
}
