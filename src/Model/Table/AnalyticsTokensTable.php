<?php
namespace FinlayDaG33k\Analytics\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AnalyticsTokens Model
 *
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken get($primaryKey, $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken newEntity($data = null, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken[] newEntities(array $data, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken[] patchEntities($entities, array $data, array $options = [])
 * @method \FinlayDaG33k\Analytics\Model\Entity\AnalyticsToken findOrCreate($search, callable $callback = null, $options = [])
 */
class AnalyticsTokensTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('analytics_tokens');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('token')
            ->requirePresence('token', 'create')
            ->notEmptyString('token');

        return $validator;
    }
}
