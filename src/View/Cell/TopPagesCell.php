<?php 
namespace FinlayDaG33k\Analytics\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class TopPagesCell extends Cell {
  private $AnalyticsData;

  public function initialize() {
    parent::initialize();

    // Get the required tables
    $this->AnalyticsData = TableRegistry::getTableLocator()->get('AnalyticsData');
  }

  public function display(){
    // Get the current time
    $now = Time::now();

    // Get this month's entries
    $entries = $this->AnalyticsData
      ->find()
      ->where([
        'date >' => $now->subMonth(1)->format('Y-m-d'),
      ])
      ->select([
        'AnalyticsData.url',
        'count' => 'COUNT(`url`)'
      ])
      ->group('AnalyticsData.url');

    // Limit the amount of pages
    $pages = $entries->limit(10);

    // Sort
    $pages = $pages->order(['count' => 'DESC']);

    $this->set(compact('pages'));
  }
}