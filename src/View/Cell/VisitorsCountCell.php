<?php 
namespace FinlayDaG33k\Analytics\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class VisitorsCountCell extends Cell {
  private $AnalyticsData;

  public function initialize() {
    parent::initialize();

    // Get the required tables
    $this->AnalyticsData = TableRegistry::getTableLocator()->get('AnalyticsData');
  }

  public function display(){
    // Get the current time
    $now = Time::now();

    // Get today's entries
    $unique_today = $this->AnalyticsData
      ->find()
      ->where([
        'date >' => $now->format('Y-m-d')
      ]);
    
    // Get today's unique entries
    $unique_today = $unique_today->select([
      'count' => $unique_today->func()->count('DISTINCT `token_id`')
    ]);

    // Get this months's entries
    $unique_month = $this->AnalyticsData
      ->find()
      ->where([
        'date >' => $now->subMonth(1)->format('Y-m-d'),
      ]);
    
    // Get this months's unique entries
    $unique_month = $unique_month->select([
      'count' => $unique_month->func()->count('DISTINCT `token_id`')
    ]);
    
    // Pass everything to the view
    $this->set('unique_today', $unique_today->toArray()[0]->count);
    $this->set('unique_month', $unique_month->toArray()[0]->count);
  }
}