<?php
  namespace FinlayDaG33k\Analytics\GraphQL\Datasource;

  use Cake\Utility\Security;
  use Cake\ORM\TableRegistry;
  use Cake\Routing\Router;
  use Cake\i18n\Time;
  use Josegonzalez\CakeQueuesadilla\Queue\Queue;

  class AnalyticsDatasource {
    private $AnalyticsTokens;
    private $request;

    public function __construct() {
      // Get this request
      $this->request = Router::getRequest();

      // Load our table
      $this->AnalyticsTokens = TableRegistry::getTableLocator()->get('AnalyticsTokens');
    }

    public function submitAction(array $args) {
      // Check if a target was set
      // If not, do not process this any further
      if(!$args['target']) return;

      // Check if a token was specified
      // If not, generate one
      if(!$args['token']) $args['token'] = $this->newToken();
      
      // Check if we now have a token
      // If not, something is up
      // Throw an error
      if(!$args['token']) throw new \Exception('No token could be found after an attempt to create it');
     
      // Get the token info
      // If a token was not found, generate a new one and re-run this method
      $user = $this->AnalyticsTokens->findByToken($args['token'])->first();
      if(!$user) {
        $args['token'] = $this->newToken();
        return $this->submitAction($args);
      }

      // Submit to queue for insertion
      Queue::push('\FinlayDaG33k\Analytics\Job\SubmissionJob::run', [
        'token' => $user->id,
        'ip' => $this->request->clientIp(),
        'user-agent' => $this->request->getHeaderLine('User-Agent'),
        'origin' => $args['origin'],
        'url' => $args['target'],
        'user-settings' => $this->request->getCookie('userSettings'),
        'date' => Time::now(),
      ]);

      // Everything went alright
      // Return our token
      return $args['token'];
    }

    public function newToken(): ?string {
      // Generate a new token string
      $token = Security::randomString(32);

      // Create our new database entity
      // Then assign the token to it
      $entity = $this->AnalyticsTokens->newEntity();
      $entity->token = $token;

      // Save the token
      if($this->AnalyticsTokens->save($entity)) return $token;
      return null;
    }
  }