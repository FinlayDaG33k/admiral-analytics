<?php
  namespace FinlayDaG33k\Analytics\GraphQL\Resolver;

  use FinlayDaG33k\Analytics\GraphQL\Datasource\AnalyticsDatasource;

  use GraphQL\Type\Definition\ResolveInfo;

  class SubmitAnalyticsActionResolver {
    protected $analyticsDatasource;

    public function resolver($rootValue, $args, $context, ResolveInfo $info) {
      // Build our method name
      $method = 'resolve' . ucfirst($info->fieldName);

      // Check if our method exists
      // If so, resolve using the method
      // Else, resolve using the field itself
      if (method_exists($this, $method)) {
        // Initialize the Datasource if not done yet
        if(!$this->analyticsDatasource) $this->analyticsDatasource = new AnalyticsDatasource();
        
        // Resolve the field and return the info
        return $this->{$method}($rootValue, $args, $context, $info);
      } else {
        return $rootValue->{$info->fieldName};
      }
    }

    public function resolveSubmitAnalyticsAction($rootValue, $args, $context, $info) {
      return $this->analyticsDatasource->submitAction($args);
    }
  }