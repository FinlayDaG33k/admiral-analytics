<div class="text-center" data-cell="finlaydag33k/analytics.visitorsCount">
  <h2><?= h($unique_today); ?></h2>
  <small>
    <b><?= h($unique_month); ?></b> Unique visitors this month
  </small>
</div>