<table class="table">
  <thead>
    <tr>
      <th scope="col">Page url</th>
      <th scope="col">Pageviews</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($pages as $page): ?>
      <tr>
        <td><?= str_replace(\Cake\Routing\Router::url('/', true), '/', $page->url); ?></td>
        <td><?= $page->count; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>