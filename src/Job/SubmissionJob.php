<?php
  namespace FinlayDaG33k\Analytics\Job;

  use Cake\ORM\TableRegistry;

  class SubmissionJob {
    private static $AnalyticsData;
    private static $hostname;
    private static $crawlers = [
      'googlebot.com',
    ];

    public static function run($job) {
      // Check whether we this is a known crawler
      self::$hostname = gethostbyaddr($job->data('ip'));
      if(self::isCrawler()) return;

      // Get our table
      self::$AnalyticsData = TableRegistry::getTableLocator()->get('AnalyticsData');

      // Create a new entity in our database
      $entity = self::$AnalyticsData->newEntity([
        'token_id' => $job->data('token'),
        'ip' => $job->data('ip'),
        'country' => 'N/A',
        'useragent' => $job->data('user-agent'),
        'origin' => $job->data('origin'),
        'url' => $job->data('url'),
        'settings' => $job->data('user-settings'),
        'date' => $job->data('date'),
      ]);

      // Insert our entity
      self::$AnalyticsData->save($entity);
    }

    private static function isCrawler() {
      // Add each crawler to a regex
      $regexp = '/(';
      $last = end(self::$crawlers);
      foreach(self::$crawlers as $crawler) {
        $regexp .= preg_quote('.' . $crawler);
        if($crawler != $last) $regexp .= '|';
      }
      $regexp .= ')$/i';

      // Execute the regex returning the result
      return preg_match($regexp, self::$hostname);
    }
  }