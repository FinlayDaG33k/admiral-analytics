# Analytics
Admiral module for collecting analytics surrounding your visitors

# Privacy
This plugin currently collects the following data:
- url users visit
- the IP
- estimated country of origin
- useragent
- datetime of the request
- the user's usersettings

This data will be linked together by means of an analytics token that is unique for each browser and does not expire with the session.  
This token, however, is confidential and cannot be linked to a user.

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | Admiral Version | CakePHP Version |
|----------------|-----------------|-----------------|
| 1.x            | 1.x-2.x         | 3.x             |