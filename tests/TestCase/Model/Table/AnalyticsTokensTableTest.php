<?php
namespace FinlayDaG33k\Analytics\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use FinlayDaG33k\Analytics\Model\Table\AnalyticsTokensTable;

/**
 * FinlayDaG33k\Analytics\Model\Table\AnalyticsTokensTable Test Case
 */
class AnalyticsTokensTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \FinlayDaG33k\Analytics\Model\Table\AnalyticsTokensTable
     */
    public $AnalyticsTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.FinlayDaG33k/Analytics.AnalyticsTokens'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AnalyticsTokens') ? [] : ['className' => AnalyticsTokensTable::class];
        $this->AnalyticsTokens = TableRegistry::getTableLocator()->get('AnalyticsTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AnalyticsTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
