<?php
namespace FinlayDaG33k\Analytics\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use FinlayDaG33k\Analytics\Model\Table\AnalyticsDataTable;

/**
 * FinlayDaG33k\Analytics\Model\Table\AnalyticsDataTable Test Case
 */
class AnalyticsDataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \FinlayDaG33k\Analytics\Model\Table\AnalyticsDataTable
     */
    public $AnalyticsData;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.FinlayDaG33k/Analytics.AnalyticsData',
        'plugin.FinlayDaG33k/Analytics.AnalyticsTokens'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AnalyticsData') ? [] : ['className' => AnalyticsDataTable::class];
        $this->AnalyticsData = TableRegistry::getTableLocator()->get('AnalyticsData', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AnalyticsData);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
