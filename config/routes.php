<?php
  use Cake\Routing\Route\DashedRoute;
  use Cake\Routing\Router;

  /**
   * Routes for the api
   */
  Router::plugin('FinlayDaG33k/Analytics',['path' => '/analytics', '_namePrefix' => 'FinlayDaG33k/Analytics.'],function ($routes) {
    $routes->get('/submit', ['controller' => 'Analytics', 'action' => 'submit', '_name' => 'submit']);

    $routes->fallbacks(DashedRoute::class);
  });