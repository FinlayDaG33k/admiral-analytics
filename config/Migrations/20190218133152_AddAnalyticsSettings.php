<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddAnalyticsSettings extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    $optionsTable = TableRegistry::get('Options');

    // Add the matomo host
    $option = $optionsTable->newEntity();
    $option->name = 'matomo_host';
    $option->value = '';
    $optionsTable->save($option);

    // Add the matomo siteid
    $option = $optionsTable->newEntity();
    $option->name = 'matomo_siteid';
    $option->value = '';
    $optionsTable->save($option);

    // Add the matomo authtoken
    $option = $optionsTable->newEntity();
    $option->name = 'matomo_token';
    $option->value = '';
    $optionsTable->save($option);
  }
}
