<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddAnalyticsData extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Create a table for the tokens
    $this->table('analytics_tokens')
      ->addColumn('token','text',['default' => null, 'null' => false])
      ->save();

    // Create a table for storing requests
    $this->table('analytics_data')
      ->addColumn('token_id', 'integer', ['default' => null, 'null' => false])
      ->addColumn('ip', 'text', ['default' => null, 'null' => false])
      ->addColumn('country', 'text', ['default' => null, 'null' => false])
      ->addColumn('useragent', 'text', ['default' => null, 'null' => false])
      ->addColumn('url', 'text', ['default' => null, 'null' => false])
      ->addColumn('settings', 'text', ['default' => null, 'null' => false])
      ->addColumn('date', 'datetime', ['default' => null, 'null' => false])
      ->addForeignKey('token_id','analytics_tokens','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
      ->save();
  }
}
