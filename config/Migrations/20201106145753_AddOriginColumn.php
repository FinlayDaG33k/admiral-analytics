<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddOriginColumn extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Create a table for storing requests
    $this->table('analytics_data')
      ->addColumn('origin', 'text', ['default' => null, 'null' => true])
      ->save();
  }
}
