<?php
use Cake\Controller\Controller;

use Admiral\GraphQL\Types;
use Admiral\GraphQL\Types\Definition\MutationType;

if(PHP_SAPI === 'fpm-fcgi'){
  $controller = new Controller();

  $controller->loadComponent('Admiral/Admiral.Dashboard');

  // Add our dashboard cells
  $controller->Dashboard->registerCell([
    'title' => __('Visitor Count'),
    'ns' => 'FinlayDaG33k/Analytics',
    'cell' => 'VisitorsCount'
  ]);
  $controller->Dashboard->registerCell([
    'title' => __('Top Pages'),
    'ns' => 'FinlayDaG33k/Analytics',
    'cell' => 'TopPages',
    'columns' => 6,
  ]);

  // Register GraphQL fields for Mutations
  MutationType::addField('submitAnalyticsAction',[
    'type' => Types::get('string'),
    'description' => 'Submit an action to the analytics database',
    'args' => [
      'origin' => Types::get('string'),
      'target' => Types::get('string'),
      'token' => Types::get('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \FinlayDaG33k\Analytics\GraphQL\Resolver\SubmitAnalyticsActionResolver)->resolver($rootValue, $args, $context, $info);
    }
  ]);
}