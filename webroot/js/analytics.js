class Analytics {
    async submit() {
      // Initialize a new GraphQL instance
      const gql = new GraphQL();

      // Build our mutation
      gql.setQuery(`mutation($origin: String, $target: String!, $token: String) {
        submitAnalyticsAction(origin: $origin, target: $target, token: $token)
      }`);

      // Get our current token
      let token;
      try {
        token = await this.getToken();
      } catch(e) {
        token = null;
      }

      // Set our variables
      gql.addVariable('origin', document.referrer)
      gql.addVariable('target', window.location.href);
      gql.addVariable('token', token);

      // Submit the analytics
      const resp = await gql.execute();
      
      // Check if our returned token equals our current one
      // If not, update it
      if(token != resp.data.submitAnalyticsAction) {
        try {
          await this.storeToken(resp.data.submitAnalyticsAction);
        } catch(e) {
          console.error(`Could not store token to localstorage!`);
        }
      }
    }
  
    /**
     * Store our token to the local storage
     * 
     * @param token 
     * @returns Promise<unknown>
     */
    storeToken(token) {
      return new Promise((resolve, reject) => {
        try {
          window.localStorage.setItem('analytics token', token);
          resolve();
        } catch(e) {
          reject(e);
        }
      });
    }
  
    getToken() {
      return new Promise((resolve, reject) => {
        try {
          const token = window.localStorage.getItem('analytics token');
          if(!token) reject(null);
          resolve(token);
        } catch(e) {
          reject(e);
        }
      });
    } 
  }